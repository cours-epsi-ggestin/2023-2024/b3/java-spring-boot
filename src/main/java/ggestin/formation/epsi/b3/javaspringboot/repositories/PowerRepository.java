package ggestin.formation.epsi.b3.javaspringboot.repositories;

import ggestin.formation.epsi.b3.javaspringboot.model.Power;
import ggestin.formation.epsi.b3.javaspringboot.model.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PowerRepository
        extends
        JpaRepository<Power, Long> {
}
