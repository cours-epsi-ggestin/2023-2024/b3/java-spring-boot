package ggestin.formation.epsi.b3.javaspringboot.services;

import ggestin.formation.epsi.b3.javaspringboot.model.Villain;

import java.util.List;
import java.util.Optional;

public interface VillainService {

    Villain saveVillain(Villain villain);

    boolean existsById(Long id);

    List<Villain> findAll();

    void deleteById(Long id);

    Optional<Villain> findById(Long id);
}
