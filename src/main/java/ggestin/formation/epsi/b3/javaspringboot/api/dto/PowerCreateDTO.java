package ggestin.formation.epsi.b3.javaspringboot.api.dto;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
public class PowerCreateDTO {
    private String name;
    private String description;
}
