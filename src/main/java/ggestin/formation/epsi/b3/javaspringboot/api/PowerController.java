package ggestin.formation.epsi.b3.javaspringboot.api;

import ggestin.formation.epsi.b3.javaspringboot.api.dto.PowerCreateDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.PowerDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.mappers.PowerMapper;
import ggestin.formation.epsi.b3.javaspringboot.model.Power;
import ggestin.formation.epsi.b3.javaspringboot.services.PowerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/powers")
@RequiredArgsConstructor
@Slf4j
public class PowerController {

    private final PowerService powerService;
    private final PowerMapper powerMapper;

    @GetMapping()
    public ResponseEntity<List<PowerDTO>> getAll(
    ) {
        return ResponseEntity.ok(this.powerMapper.toDtos(this.powerService.findAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PowerDTO> getById(
            @PathVariable() Long id
    ) {
        return ResponseEntity.of(
                this.powerService.findById(id).map(this.powerMapper::toDto)
        );
    }

    @PostMapping()
    public ResponseEntity<PowerDTO> createPower(
            @RequestBody PowerCreateDTO powerCreateDTO
    ) {
        Power createdPower = powerService.savePower(
                this.powerMapper.fromDto(powerCreateDTO)
        );

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(this.powerMapper.toDto(createdPower));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PowerDTO> updatePower(
            @PathVariable() Long id,
            @RequestBody PowerDTO powerDTO
    ) {
        if (!id.equals(powerDTO.getId())) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }

        if (!this.powerService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        ;

        Power updatedPower = powerService.savePower(
                this.powerMapper.fromDto(powerDTO)
        );
        return ResponseEntity
                .ok(this.powerMapper.toDto(updatedPower));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePower(
            @PathVariable() Long id
    ) {
        if (!this.powerService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        ;

        this.powerService.deleteById(id);

        if (this.powerService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }

        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build();
    }
}
